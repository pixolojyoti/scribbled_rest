<?php 
 defined('BASEPATH') OR exit('No direct script access allowed'); 
 
 header('Access-Control-Allow-Origin: *'); 
 
 class User_devices extends PIXOLO_Controller { 
 
 	 function __construct(){ 
 	 	 parent::__construct(); 
 
 	 	 $this->load->model('User_device_model', 'model'); 
 	 } 

 	 public function index() 
 	 { 
 	 	 $message['json']=$this->model->get_all(); 
 	 	 $this->load->view('json', $message); 
 	 }

     public function adddevice()
     {
         $userid = $this->input->get("user_id");
         $deviceid = $this->input->get("deviceid");
         $message['json']=$this->model->adddevice($userid, $deviceid);
         $this->load->view('json', $message);
     }
 }