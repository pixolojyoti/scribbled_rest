<?php 
 defined('BASEPATH') OR exit('No direct script access allowed'); 
 
 header('Access-Control-Allow-Origin: *'); 
 
 class Users extends PIXOLO_Controller { 
 
 	 function __construct(){ 
 	 	 parent::__construct(); 
 
 	 	 $this->load->model('User_Model', 'model'); 
 	 } 

 	 public function index()
     {
         $message['json'] = $this->model->get_all();
         $this->load->view('json', $message);
     }

     public function checkcontact(){
 	     $contact = $this->input->get("contact");
         $message['json'] = $this->model->checkcontact($contact);
         $this->load->view('json', $message);
     }

     public function getprofiledata(){
 	     $userid = $this->input->get("user_id");
         $myid = $this->input->get("my_id");
 	     $message['json'] = $this->model->getprofiledata($userid, $myid);
         $this->load->view('json', $message);
     }

     public function getuserfollowers(){
 	     $userid = $this->input->get("user_id");
         $myid = $this->input->get("my_id");
         $message['json'] = $this->model->getuserfollowers($userid, $myid);
         $this->load->view('json', $message);
     }

     public function getuserfollowed(){
         $userid = $this->input->get("user_id");
         $myid = $this->input->get("my_id");
         $message['json'] = $this->model->getuserfollowed($userid, $myid);
         $this->load->view('json', $message);
     }

     public function register()
     {
         $name = $this->input->get("name");
         $contact = $this->input->get("contact");
         $email = $this->input->get("email");
         $password = $this->input->get("password");
         $deviceid = $this->input->get("deviceid");
         $message['json'] = $this->model->register($name, $contact, $email, $password, $deviceid);
         $this->load->view('json', $message);
     }

     public function logout()
     {
         $userid = $this->input->get("user_id");
         $deviceid = $this->input->get("deviceid");
         $message['json'] = $this->model->logout($userid, $deviceid);
         $this->load->view('json', $message);
     }
 }