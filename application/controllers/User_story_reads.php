<?php 
 defined('BASEPATH') OR exit('No direct script access allowed'); 
 
 header('Access-Control-Allow-Origin: *'); 
 
 class User_story_reads extends PIXOLO_Controller { 
 
 	 function __construct(){ 
 	 	 parent::__construct(); 
 
 	 	 $this->load->model('User_story_read_model', 'model'); 
 	 } 

 	 public function index() 
 	 { 
 	 	 $message['json']=$this->model->get_all(); 
 	 	 $this->load->view('json', $message); 
 	 }

 	 public function userreadstory()
     {
         $userid = $this->input->get("user_id");
         $storyid = $this->input->get("story_id");
         $message['json']=$this->model->userreadstory($userid, $storyid);
         $this->load->view('json', $message);
     }
 }