<?php
defined('BASEPATH') OR exit('No direct script access allowed');

header('Access-Control-Allow-Origin: *');

class Stories extends PIXOLO_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('Storie_model', 'model');
    }

    public function index()
    {
        $message['json'] = $this->model->get_all();
        $this->load->view('json', $message);
    }

    public function getfeed()
    {
        $userid = $this->input->get("user_id");
        $laststoryid=$this->input->get("last_story_id");
        $message['json']=$this->model->getfeed($userid,$laststoryid);
        $this->load->view('json', $message);
    }
    public function getchatstories()
    {
        $userid = $this->input->get("user_id");
        $laststoryid=$this->input->get("last_story_id");
        $message['json']=$this->model->getchatstories($userid,$laststoryid);
        $this->load->view('json', $message);
    }

    public function getcontributors()
    {
        $storyid = $this->input->get("story_id");
        $myid = $this->input->get("my_id");
        $message['json']=$this->model->getcontributors($storyid, $myid);
        $this->load->view('json', $message);
    }

    public function getuserbookmarkedstories()
    {
        $userid = $this->input->get("user_id");
        $laststoryid=$this->input->get("last_story_id");
        $message['json']=$this->model->getuserbookmarkedstories($userid,$laststoryid);
        $this->load->view('json', $message);
    }

    public function getusercreatedstories()
    {
        $userid = $this->input->get("user_id");
        $message['json']=$this->model->getusercreatedstories($userid);
        $this->load->view('json', $message);
    }

    public function getusercontributedstories()
    {
        $userid = $this->input->get("user_id");
        $message['json']=$this->model->getusercontributedstories($userid);
        $this->load->view('json', $message);
    }

    public function getmystories(){
        $userid = $this->input->get("user_id");
        $laststoryid=$this->input->get("last_story_id");
        $message['json']=$this->model->getmystories($userid,$laststoryid);
        $this->load->view('json', $message);
    }


    public function getstorybyid(){
        $id = $this->input->get("id");
        $userid = $this->input->get("user_id");
        $message['json']=$this->model->getstorybyid($id,$userid);
        $this->load->view('json', $message);

    }

    public function endstory(){
        $id = $this->input->get("id");
        $message['json']=$this->model->endstory($id);
        $this->load->view('json', $message);
    }
}