<?php 
 defined('BASEPATH') OR exit('No direct script access allowed'); 
 
 header('Access-Control-Allow-Origin: *'); 
 
 class Story_cards extends PIXOLO_Controller { 
 
 	 function __construct(){ 
 	 	 parent::__construct(); 
 
 	 	 $this->load->model('Story_card_model', 'model'); 
 	 } 

 	 public function index() 
 	 { 
 	 	 $message['json']=$this->model->get_all(); 
 	 	 $this->load->view('json', $message); 
 	 }

 	 public function getcardsbystoryid()
     {
         $storyid = $this->input->get("story_id");
         $userid = $this->input->get("user_id");
         $message['json']=$this->model->getcardsbystoryid($storyid, $userid);
         $this->load->view('json', $message);
     }
 }