
<?php 
 defined('BASEPATH') OR exit('No direct script access allowed'); 
 
 class Storie_model extends PIXOLO_Model 
 { 

 
 	 //Write functions here
     public function getfeed($userid,$laststoryid)
     {
         $query = $this->db->query("SELECT `s`.`id` AS `id`,`s`.`storytype_id` AS `storytypeid`, `s`.`title` AS `title`, `s`.`image` AS `image`, `s`.`category_id` AS `categoryid`,`c`.`name` AS `categoryname`, `s`.`user_id` AS `creatorid`, `s`.`iscomplete` AS `iscomplete`, `s`.`createdat` AS `createdat`, `s`.`updatedat` AS `updatedat`, `u`.`name`, IFNULL(`ult`.`totallikes`,0) AS `totallikes` , count(`usv`.`id`) AS `totalviews`, IF(EXISTS (SELECT `user_likes`.`id` FROM `user_likes` WHERE `story_id` = `s`.`id` AND `user_id` = '$userid') , 1 , 0) AS `likedbyme`, IF(EXISTS (SELECT `user_story_bookmarks`.`id` FROM `user_story_bookmarks` WHERE `story_id` = `s`.`id` AND `user_id` = '$userid') , 1 , 0) AS `bookmarkedbyme`, IF(EXISTS (SELECT `user_story_reads`.`id` FROM `user_story_reads` WHERE `story_id` = `s`.`id` AND `user_id` = '$userid') , 1 , 0) AS `viewedbyme`, IFNULL((SELECT `rating` FROM `user_story_ratings` WHERE `story_id` = `s`.`id` AND `user_id` = '$userid'),0) AS `myrating`, IFNULL(`usrt`.`totalrating`,0) AS `totalrating` FROM `stories` `s` INNER JOIN `categories` `c` ON `s`.`category_id`=`c`.`id` LEFT JOIN `users` `u` ON `s`.`user_id` = `u`.`id` LEFT JOIN `user_story_views` `usv` ON `s`.`id` = `usv`.`story_id` LEFT JOIN (SELECT count(`ul`.`id`) AS `totallikes`, `ul`.`story_id` AS `ulsid` FROM `user_likes` `ul` GROUP BY `ulsid`) AS `ult` ON `s`.`id` = `ult`.`ulsid`  LEFT JOIN ( SELECT AVG(`usr`.`rating`) AS `totalrating`, `usr`.`story_id` AS `story_id` FROM `user_story_ratings` `usr` GROUP BY `usr`.`story_id`) AS `usrt` ON `s`.`id` = `usrt`.`story_id` WHERE `s`.`isactive` = '1' and `s`.`storytype_id`=1 and `s`.`id`>$laststoryid GROUP BY `s`.`id` ORDER BY `s`.`updatedat` LIMIT 10")->result();
         foreach($query as $q){
             $q->createdat = $this->time_elapsed_string($q->createdat);
             $q->updatedat = $this->time_elapsed_string($q->updatedat);
         }
         return $query;
     }
     
     
     
     
     
          public function getchatstories($userid,$laststoryid)
     {
         $query = $this->db->query("SELECT `s`.`id` AS `id`, `s`.`title` AS `title`,`s`.`storytype_id` AS `storytypeid` ,`s`.`image` AS `image`, `s`.`category_id` AS `categoryid`,`c`.`name` AS `categoryname`, `s`.`user_id` AS `creatorid`, `s`.`iscomplete` AS `iscomplete`, `s`.`createdat` AS `createdat`, `s`.`updatedat` AS `updatedat`, `u`.`name`, IFNULL(`ult`.`totallikes`,0) AS `totallikes` , count(`usv`.`id`) AS `totalviews`, IF(EXISTS (SELECT `user_likes`.`id` FROM `user_likes` WHERE `story_id` = `s`.`id` AND `user_id` = '$userid') , 1 , 0) AS `likedbyme`, IF(EXISTS (SELECT `user_story_bookmarks`.`id` FROM `user_story_bookmarks` WHERE `story_id` = `s`.`id` AND `user_id` = '$userid') , 1 , 0) AS `bookmarkedbyme`, IF(EXISTS (SELECT `user_story_reads`.`id` FROM `user_story_reads` WHERE `story_id` = `s`.`id` AND `user_id` = '$userid') , 1 , 0) AS `viewedbyme`, IFNULL((SELECT `rating` FROM `user_story_ratings` WHERE `story_id` = `s`.`id` AND `user_id` = '$userid'),0) AS `myrating`, IFNULL(`usrt`.`totalrating`,0) AS `totalrating` FROM `stories` `s` LEFT JOIN `categories` `c` ON `s`.`category_id`=`c`.`id` LEFT JOIN `users` `u` ON `s`.`user_id` = `u`.`id` LEFT JOIN `user_story_views` `usv` ON `s`.`id` = `usv`.`story_id` LEFT JOIN (SELECT count(`ul`.`id`) AS `totallikes`, `ul`.`story_id` AS `ulsid` FROM `user_likes` `ul` GROUP BY `ulsid`) AS `ult` ON `s`.`id` = `ult`.`ulsid`  LEFT JOIN ( SELECT AVG(`usr`.`rating`) AS `totalrating`, `usr`.`story_id` AS `story_id` FROM `user_story_ratings` `usr` GROUP BY `usr`.`story_id`) AS `usrt` ON `s`.`id` = `usrt`.`story_id` WHERE `s`.`isactive` = '1' and `s`.`storytype_id`='2' and `s`.`id`> $laststoryid GROUP BY `s`.`id` ORDER BY `s`.`updatedat` LIMIT 10")->result();
         foreach($query as $q){
             $q->createdat = $this->time_elapsed_string($q->createdat);
             $q->updatedat = $this->time_elapsed_string($q->updatedat);
         }
         return $query;
     }
     

     public function getcontributors($storyid, $myid){
         $query = $this->db->query("SELECT `u`.`name` AS `name`, `u`.`id` AS `id`, `u`.`image` AS `image`, count(`u`.`id`) AS `totalcontributions`, `u`.`followerscount` AS `followerscount`, `u`.`followingcount` AS `followingcount`,IFNULL((SELECT count(`id`) FROM `story_cards` WHERE `user_id` = `u`.`id`),0) AS `totalcontributions`, IF(EXISTS(SELECT `id` FROM `user_follows` WHERE `follower_user_id` = '$myid' AND `followed_user_id` = `u`.`id`), 1, 0) AS `isfollowing` FROM `story_cards` `sc` INNER JOIN `users` `u` ON `sc`.`user_id` = `u`.`id` WHERE `sc`.`story_id` = '$storyid' GROUP BY `u`.`id`")->result();
         return $query;
     }

     public function getuserbookmarkedstories($userid,$laststoryid){
         $query = $this->db->query("SELECT `s`.`id` AS `id`, `s`.`title` AS `title`, `s`.`image` AS `image`, `s`.`category_id` AS `categoryid`, `s`.`user_id` AS `creatorid`, `s`.`iscomplete` AS `iscomplete`, `s`.`createdat` AS `createdat`, `s`.`updatedat` AS `updatedat`, `u`.`name`, IFNULL(`ult`.`totallikes`,0) AS `totallikes` , count(`usv`.`id`) AS `totalviews`, IF(EXISTS (SELECT `user_likes`.`id` FROM `user_likes` WHERE `story_id` = `s`.`id` AND `user_id` = '$userid') , 1 , 0) AS `likedbyme`, IF(EXISTS (SELECT `user_story_bookmarks`.`id` FROM `user_story_bookmarks` WHERE `story_id` = `s`.`id` AND `user_id` = '$userid') , 1 , 0) AS `bookmarkedbyme`, IF(EXISTS (SELECT `user_story_reads`.`id` FROM `user_story_reads` WHERE `story_id` = `s`.`id` AND `user_id` = '$userid') , 1 , 0) AS `viewedbyme`, IFNULL((SELECT `rating` FROM `user_story_ratings` WHERE `story_id` = `s`.`id` AND `user_id` = '$userid'),0) AS `myrating`, IFNULL(`usrt`.`totalrating`,0) AS `totalrating` FROM `stories` `s` INNER JOIN `user_story_bookmarks` `usb` ON `s`.`id` = `usb`.`story_id` LEFT JOIN `users` `u` ON `s`.`user_id` = `u`.`id` LEFT JOIN `user_story_views` `usv` ON `s`.`id` = `usv`.`story_id` LEFT JOIN (SELECT count(`ul`.`id`) AS `totallikes`, `ul`.`story_id` AS `ulsid` FROM `user_likes` `ul` GROUP BY `ulsid`) AS `ult` ON `s`.`id` = `ult`.`ulsid`  LEFT JOIN ( SELECT AVG(`usr`.`rating`) AS `totalrating`, `usr`.`story_id` AS `story_id` FROM `user_story_ratings` `usr` GROUP BY `usr`.`story_id`) AS `usrt` ON `s`.`id` = `usrt`.`story_id` WHERE `s`.`isactive` = '1' AND `usb`.`user_id` = '$userid' and `s`.`id`> $laststoryid GROUP BY `s`.`id` ORDER BY `s`.`updatedat` LIMIT 10")->result();
         foreach($query as $q){
             $q->createdat = $this->time_elapsed_string($q->createdat);
             $q->updatedat = $this->time_elapsed_string($q->updatedat);
         }
         return $query;
     }


     public function getusercreatedstories($userid){
         $query = $this->db->query("SELECT `s`.`id` AS `id`, `s`.`title` AS `title`, `s`.`image` AS `image`, `s`.`category_id` AS `categoryid`, `s`.`user_id` AS `creatorid`, `s`.`iscomplete` AS `iscomplete`, `s`.`createdat` AS `createdat`, `s`.`updatedat` AS `updatedat`, `u`.`name`, IFNULL(`ult`.`totallikes`,0) AS `totallikes` , count(`usv`.`id`) AS `totalviews`, IF(EXISTS (SELECT `user_likes`.`id` FROM `user_likes` WHERE `story_id` = `s`.`id` AND `user_id` = '$userid') , 1 , 0) AS `likedbyme`, IF(EXISTS (SELECT `user_story_bookmarks`.`id` FROM `user_story_bookmarks` WHERE `story_id` = `s`.`id` AND `user_id` = '$userid') , 1 , 0) AS `bookmarkedbyme`, IF(EXISTS (SELECT `user_story_reads`.`id` FROM `user_story_reads` WHERE `story_id` = `s`.`id` AND `user_id` = '$userid') , 1 , 0) AS `viewedbyme`, IFNULL((SELECT `rating` FROM `user_story_ratings` WHERE `story_id` = `s`.`id` AND `user_id` = '$userid'),0) AS `myrating`, IFNULL(`usrt`.`totalrating`,0) AS `totalrating` FROM `stories` `s` LEFT JOIN `users` `u` ON `s`.`user_id` = `u`.`id` LEFT JOIN `user_story_views` `usv` ON `s`.`id` = `usv`.`story_id` LEFT JOIN (SELECT count(`ul`.`id`) AS `totallikes`, `ul`.`story_id` AS `ulsid` FROM `user_likes` `ul` GROUP BY `ulsid`) AS `ult` ON `s`.`id` = `ult`.`ulsid`  LEFT JOIN ( SELECT AVG(`usr`.`rating`) AS `totalrating`, `usr`.`story_id` AS `story_id` FROM `user_story_ratings` `usr` GROUP BY `usr`.`story_id`) AS `usrt` ON `s`.`id` = `usrt`.`story_id` WHERE `s`.`isactive` = '1' AND `s`.`user_id` = '$userid' GROUP BY `s`.`id` ORDER BY `s`.`updatedat`")->result();
         foreach($query as $q){
             $q->createdat = $this->time_elapsed_string($q->createdat);
             $q->updatedat = $this->time_elapsed_string($q->updatedat);
         }
         return $query;
     }

     public function getusercontributedstories($userid){
         $query = $this->db->query("SELECT `s`.`id` AS `id`, `s`.`title` AS `title`, `s`.`image` AS `image`, `s`.`category_id` AS `categoryid`, `s`.`user_id` AS `creatorid`, `s`.`iscomplete` AS `iscomplete`, `s`.`createdat` AS `createdat`, `s`.`updatedat` AS `updatedat`, `u`.`name`, IFNULL(`ult`.`totallikes`,0) AS `totallikes` , count(`usv`.`id`) AS `totalviews`, IF(EXISTS (SELECT `user_likes`.`id` FROM `user_likes` WHERE `story_id` = `s`.`id` AND `user_id` = '$userid') , 1 , 0) AS `likedbyme`, IF(EXISTS (SELECT `user_story_bookmarks`.`id` FROM `user_story_bookmarks` WHERE `story_id` = `s`.`id` AND `user_id` = '$userid') , 1 , 0) AS `bookmarkedbyme`, IF(EXISTS (SELECT `user_story_reads`.`id` FROM `user_story_reads` WHERE `story_id` = `s`.`id` AND `user_id` = '$userid') , 1 , 0) AS `viewedbyme`, IFNULL((SELECT `rating` FROM `user_story_ratings` WHERE `story_id` = `s`.`id` AND `user_id` = '$userid'),0) AS `myrating`, IFNULL(`usrt`.`totalrating`,0) AS `totalrating` FROM `stories` `s` INNER JOIN (SELECT DISTINCT `sc`.`story_id` AS `sid` FROM `story_cards` `sc` WHERE `sc`.`user_id` = '$userid') AS `sct` ON `s`.`id` = `sct`.`sid` LEFT JOIN `users` `u` ON `s`.`user_id` = `u`.`id` LEFT JOIN `user_story_views` `usv` ON `s`.`id` = `usv`.`story_id` LEFT JOIN (SELECT count(`ul`.`id`) AS `totallikes`, `ul`.`story_id` AS `ulsid` FROM `user_likes` `ul` GROUP BY `ulsid`) AS `ult` ON `s`.`id` = `ult`.`ulsid`  LEFT JOIN ( SELECT AVG(`usr`.`rating`) AS `totalrating`, `usr`.`story_id` AS `story_id` FROM `user_story_ratings` `usr` GROUP BY `usr`.`story_id`) AS `usrt` ON `s`.`id` = `usrt`.`story_id` WHERE `s`.`isactive` = '1' GROUP BY `s`.`id` ORDER BY `s`.`updatedat`")->result();
         foreach($query as $q){
             $q->createdat = $this->time_elapsed_string($q->createdat);
             $q->updatedat = $this->time_elapsed_string($q->updatedat);
         }
         return $query;
     }

     public function getmystories($userid,$laststoryid){




     }

     public function getstorybyid($id,$userid){
         $query = $this->db->query("SELECT `s`.`id` AS `id`, `s`.`title` AS `title`,`s`.`storytype_id` AS `storytypeid` ,`s`.`image` AS `image`, `s`.`category_id` AS `categoryid`,`c`.`name` AS `categoryname`, `s`.`user_id` AS `creatorid`, `s`.`iscomplete` AS `iscomplete`, `s`.`createdat` AS `createdat`, `s`.`updatedat` AS `updatedat`, `u`.`name`,(SELECT COUNT( DISTINCT  `user_id`) FROM `story_cards` WHERE `story_id`='$id'  ) AS `contributors` , IFNULL((SELECT `rating` FROM `user_story_ratings` WHERE `story_id` = `s`.`id` AND `user_id` = '$userid'),0) AS `myrating` FROM `stories` `s` LEFT JOIN `categories` `c` ON `s`.`category_id`=`c`.`id` LEFT JOIN `users` `u` ON `s`.`user_id` = `u`.`id`  WHERE  `s`.`id`= '$id' ")->row();

//         $query->createdat = $this->time_elapsed_string($query->createdat);
//         $query->updatedat = $this->time_elapsed_string($query->updatedat);

         return $query;


     }

     public function endstory($id){
         $query = $this->db->query("UPDATE `stories` SET `iscomplete`='1' WHERE `id` = $id");
         return $query;
     }

 }

?>