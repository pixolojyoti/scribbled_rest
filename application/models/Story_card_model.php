<?php 
 defined('BASEPATH') OR exit('No direct script access allowed'); 
 
 class Story_card_model extends PIXOLO_Model 
 { 

 
 	 //Write functions here
     public function getcardsbystoryid($storyid, $userid){
         $query = $this->db->query("SELECT `sc`.`id` AS `cardid`, `u`.`id` AS `userid`, `u`.`name` AS `username`, `u`.`image` AS `userimage`, `sc`.`content` AS `cardcontent`, IFNULL(`uclt`.`totallikes`,0) AS `totallikes` , `sc`.`cardnumber` AS `cardnumber`, IF(EXISTS (SELECT `user_card_likes`.`id` FROM `user_card_likes` WHERE `card_id` = `sc`.`id` AND `user_id` = '$userid') , 1 , 0) AS `likedbyme` FROM `story_cards` `sc` INNER JOIN `users` `u` ON `sc`.`user_id` = `u`.`id` LEFT JOIN (SELECT count(`ucl`.`id`) AS `totallikes`, `ucl`.`card_id` AS `uclcid` FROM `user_card_likes` `ucl` GROUP BY `ucl`.`card_id` ) AS `uclt` ON `sc`.`id` = `uclt`.`uclcid` WHERE `story_id` = '$storyid' AND `sc`.`isactive` = '1' ORDER BY `sc`.`cardnumber`
")->result();
         return $query;
     }
 } 
 
 ?>