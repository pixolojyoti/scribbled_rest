<?php 
 defined('BASEPATH') OR exit('No direct script access allowed'); 
 
 class User_Model extends PIXOLO_Model 
 { 

 
 	 //Write functions here
     public function checkcontact($contact){
         $query = $this->db->query("SELECT `id` FROM `users` WHERE `contact` = '$contact'")->row();
         if($query == null){
             return false;
         };
         return true;
     }

     public function getprofiledata($userid, $myid){
<<<<<<< HEAD
        $query = $this->db->query("SELECT `u`.`id` AS `id`,`u`.`name` AS `name`, `u`.`contact` AS `contact`, `u`.`image` AS `image`, `u`.`followerscount` AS `followers`, `u`.`followingcount` AS `following`, IFNULL(`st`.`totalstories`,0) AS `storiescreated`, IFNULL(`sct`.`totalstoriescontributedto`,0) AS `totalstoriescontributedto`, IFNULL(`sct`.`totalcontributions`,0) AS `totalcontributions`, IF(EXISTS(SELECT `id` FROM `user_follows` WHERE `follower_user_id` = '$myid' AND `followed_user_id` = `u`.`id`), 1, 0) AS `isfollowing` FROM `users` `u` LEFT JOIN (SELECT count(`s`.`id`) AS `totalstories`, `s`.`user_id` AS `uid`  FROM `stories` `s` WHERE `s`.`user_id` = '$userid' AND `s`.`isactive` = '1') AS `st` ON `u`.`id` = `st`.`uid` LEFT JOIN (SELECT count(`sc`.`id`) AS `totalcontributions`, count(DISTINCT `sc`.`story_id`) AS `totalstoriescontributedto`, `sc`.`user_id` AS `uid` FROM `story_cards` `sc` WHERE `sc`.`user_id` = '$userid') AS `sct` ON `u`.`id` = `sct`.`uid` WHERE `u`.`id` = '$userid'")->row();
=======
        $query = $this->db->query("SELECT `u`.`id` AS `id`,`u`.`name` AS `name`, `u`.`contact` AS `contact`, `u`.`image` AS `image`, `u`.`followerscount` AS `followers`, `u`.`followingcount` AS `following`, `st`.`totalstories` AS `storiescreated`, `sct`.`totalstoriescontributedto` AS `totalstoriescontributedto`, `sct`.`totalcontributions` AS `totalcontributions`, IF(EXISTS(SELECT `id` FROM `user_follows` WHERE `follower_user_id` = '$myid' AND `followed_user_id` = `u`.`id`), 1, 0) AS `isfollowing` FROM `users` `u` LEFT JOIN (SELECT count(`s`.`id`) AS `totalstories`, `s`.`user_id` AS `uid` FROM `stories` `s` WHERE `s`.`user_id` = '$userid' AND `s`.`isactive` = '1') AS `st` ON `u`.`id` = `st`.`uid` LEFT JOIN (SELECT count(`sc`.`id`) AS `totalcontributions`, count(DISTINCT `sc`.`story_id`) AS `totalstoriescontributedto`, `sc`.`user_id` AS `uid` FROM `story_cards` `sc` WHERE `sc`.`user_id` = '$userid') AS `sct` ON `u`.`id` = `sct`.`uid` WHERE `u`.`id` = '$userid'")->row();
>>>>>>> d326ceb20ae1bf6b92375ce8a1aff5b99aa28b02
        return $query;
     }

    public function getuserfollowers($userid, $myid){
        $query = $this->db->query("SELECT `u`.`id` AS `id`, `u`.`name` AS `name`, `u`.`image` AS `image`, `u`.`followerscount` AS `followerscount`, `u`.`followingcount` AS `followingcount`, IFNULL((SELECT count(`id`) FROM `story_cards` WHERE `user_id` = `u`.`id`),0) AS `totalcontributions`, IF(EXISTS(SELECT `id` FROM `user_follows` WHERE `follower_user_id` = '$myid' AND `followed_user_id` = `u`.`id`), 1, 0) AS `isfollowing` FROM `users` `u` LEFT JOIN `user_follows` `uf` ON `u`.`id` = `uf`.`follower_user_id` WHERE `uf`.`followed_user_id` = '$userid'")->result();
        return $query;
    }

     public function getuserfollowed($userid, $myid){
         $query = $this->db->query("SELECT `u`.`id` AS `id`, `u`.`name` AS `name`, `u`.`image` AS `image`, `u`.`followerscount` AS `followerscount`, `u`.`followingcount` AS `followingcount`, IFNULL((SELECT count(`id`) FROM `story_cards` WHERE `user_id` = `u`.`id`),0) AS `totalcontributions`, IF(EXISTS(SELECT `id` FROM `user_follows` WHERE `follower_user_id` = '$myid' AND `followed_user_id` = `u`.`id`), 1, 0) AS `isfollowing` FROM `users` `u` LEFT JOIN `user_follows` `uf` ON `u`.`id` = `uf`.`followed_user_id` WHERE `uf`.`follower_user_id` = '$userid'")->result();
         return $query;
     }

     public function register($name, $contact, $email, $password, $deviceid){
         $query = $this->db->query("INSERT INTO `users` (`name`, `contact`, `email`, `password`) VALUES ('$name', '$contact', '$email', '$password')");
         $userid = $this->db->insert_id();
         $query2 = $this->db->query("INSERT INTO `user_devices` (`user_id`, `deviceid`) VALUE ('$userid', '$deviceid')");
         $return = $this->db->query("SELECT * FROM `users` WHERE `id`='$userid'")->row();
         return $return;
     }

     public function logout($userid, $deviceid){
        $query = $this->db->query("UPDATE `user_device` SET `isactive` = '0' WHERE `user_id` = '$userid' AND `deviceid` = '$deviceid'");
        return true;
     }

 } 
 
 ?>